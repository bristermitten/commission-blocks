/*
 * MIT License
 *
 * Copyright (c) 2020 Alexander Wood
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package me.bristermitten.blocks.config

import me.bristermitten.blocks.lang.stripHumanColors

data class BlocksConfig(
        var database: DBConfig = DBConfig(),
        var saveInterval: Int = 5,
        var loreFormat: String = "<number>",
        var legacyLoreFormat: String? = null,
)
{

    @delegate:Transient
    val loreFormatRegex by lazy {
        loreFormat.escapeRegex().stripHumanColors().replace("number", "[0-9]+").toRegex()
    }

    @delegate:Transient
    val loreFormatSurroundingRegex by lazy {
        ("[" + loreFormat.stripHumanColors().replace("number", "") + "]").toRegex()
    }

    @delegate:Transient
    val legacyFormatRegex by lazy {
        legacyLoreFormat?.escapeRegex()?.stripHumanColors()?.replace("number", "[0-9]+")?.toRegex()
    }

    @delegate:Transient
    val legacySurroundingFormatRegex by lazy {
        ("[" + legacyLoreFormat?.escapeRegex()?.stripHumanColors()?.replace("number", "") + "]").toRegex()
    }

    private fun String.escapeRegex(): String
    {
        return replace("[", """\[""")
                .replace("]", """\]""")
    }

}
