/*
 * MIT License
 *
 * Copyright (c) 2020 Alexander Wood
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package me.bristermitten.blocks.event

import me.bristermitten.blocks.data.PlayerDataStorage
import me.clip.ezblocks.BreakHandler
import me.clip.ezblocks.EZBlocks
import org.bukkit.Bukkit
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerQuitEvent

class JoinLeaveListener(private val dataStorage: PlayerDataStorage) : Listener
{
    private val ezBlocksLoaded by lazy {
        Bukkit.getPluginManager().isPluginEnabled("EZBlocks")
    }

    @EventHandler
    fun PlayerJoinEvent.onJoin()
    {
        if (ezBlocksLoaded)
        {
            val broken = EZBlocks.getEZBlocks().getBlocksBroken(player)
            dataStorage.queueUpdate(dataStorage[player]) {
                if (broken > 0)
                {
                    it.blocks = broken.toLong()
                }
                BreakHandler.breaks.remove(player.uniqueId.toString())
            }
        } else
        {
            dataStorage.queueUpdate(dataStorage[player])
        }
    }

    @EventHandler
    fun PlayerQuitEvent.onLeave()
    {
        dataStorage.save(dataStorage[player])
    }
}
