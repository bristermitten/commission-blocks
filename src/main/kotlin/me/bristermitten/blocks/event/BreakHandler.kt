/*
 * MIT License
 *
 * Copyright (c) 2020 Alexander Wood
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package me.bristermitten.blocks.event

import me.bristermitten.blocks.config.BlocksConfig
import me.bristermitten.blocks.data.PlayerDataStorage
import me.bristermitten.blocks.lang.color
import me.bristermitten.blocks.lang.stripColor
import me.bristermitten.blocks.placeholders.formatWithCommas
import me.bristermitten.blocks.placeholders.stripCommas
import org.bukkit.Material
import org.bukkit.entity.Player

/**
 * This regex matches a number, but not a number that is part of a color code.
 */
private val NUMBERS_REGEX = "(?<!(§))[0-9,]+".toRegex()

class BreakHandler(
        private val dataStorage: PlayerDataStorage,
        private val config: BlocksConfig,
)
{

    fun handleBreak(player: Player, blockCount: Int)
    {
        applyBlockAdditionToItemInHand(player, blockCount.toLong())

        val data = dataStorage[player]
        data.blocks += blockCount
    }


    private fun applyBlockAdditionToItemInHand(player: Player, amount: Long)
    {
        val heldSlot = player.inventory.heldItemSlot
        val hand = player.inventory.getItem(heldSlot) ?: return
        if (hand.type == Material.AIR) return
        if (!hand.type.name.contains("PICKAXE")) return

        val meta = hand.itemMeta
        val lore = meta.lore ?: mutableListOf()


        var isLegacyFormat = false
        var matchingLoreIndex = lore.indexOfFirst {
            val stripped = it.stripColor().stripCommas()
            if (config.legacyFormatRegex?.matches(stripped) == true)
            {
                isLegacyFormat = true
                true
            } else config.loreFormatRegex.matches(stripped)
        }

        val surroundingRegex = if (isLegacyFormat)
        {
            config.legacySurroundingFormatRegex
        } else
        {
            config.loreFormatSurroundingRegex
        }

        if (matchingLoreIndex == -1)
        {
            lore.add(config.loreFormat.replace("number", 0.toString()).color())
            matchingLoreIndex = lore.size - 1
        }

        val currentLoreString = lore[matchingLoreIndex]
        val strippedLoreString = currentLoreString.stripColor().stripCommas().replace(surroundingRegex, "")
        val blocksBroken = strippedLoreString.toLongOrNull() ?: 0
        val newBlocksBroken = blocksBroken + amount


        val newLoreString = if (isLegacyFormat)
        {
            config.loreFormat.replace("number", newBlocksBroken.toString()).color()
        } else
        {
            currentLoreString.replace(NUMBERS_REGEX, newBlocksBroken.formatWithCommas())
        }

        lore[matchingLoreIndex] = newLoreString

        meta.lore = lore
        hand.itemMeta = meta

        player.inventory.setItem(heldSlot, hand)
    }
}
