/*
 * MIT License
 *
 * Copyright (c) 2020 Alexander Wood
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package me.bristermitten.blocks.commands

import co.aikar.commands.BaseCommand
import co.aikar.commands.annotation.*
import co.aikar.commands.bukkit.contexts.OnlinePlayer
import me.bristermitten.blocks.data.PlayerDataStorage
import me.bristermitten.blocks.lang.LangKeys
import me.bristermitten.blocks.lang.LangService
import org.bukkit.OfflinePlayer
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

@CommandAlias("blocks|bblocks")
class BlocksCommand(private val dataStorage: PlayerDataStorage, private val langService: LangService) : BaseCommand()
{
    @Default
    fun getBlocks(sender: Player)
    {
        langService.sendFormattedMessage(sender, LangKeys.BLOCKS_BROKEN)
    }

    @Default
    fun getBlocks(sender: CommandSender, target: OfflinePlayer)
    {
        val formattedMessage = langService.getFormattedMessage(LangKeys.OTHER_BLOCKS_BROKEN, target)
        sender.sendMessage(formattedMessage)
    }


    @Subcommand("set")
    @CommandPermission("blocks.set")
    @CommandCompletion("@players")
    fun setBlocks(sender: CommandSender, target: OnlinePlayer, amount: Long)
    {
        dataStorage[target.player].blocks = amount
        val formattedMessage = langService.getFormattedMessage(LangKeys.OTHER_BLOCKS_BROKEN, target.player)
        sender.sendMessage(formattedMessage)
    }
//
//    @Subcommand("clean")
//    @CommandPermission("blocks.clean")
//    @Description("Clear your pickaxe's \"blocks broken\" NBT tag. If the lore becomes out of sync, try this command.")
//    fun cleanPickaxe(sender: Player)
//    {
//        val itemInHand = sender.itemInHand
//        if (itemInHand.type == Material.AIR)
//        {
//            sender.sendMessage("${RED}You can't clean air!")
//            return
//        }
//
//        val nbtItem = NBTItem(itemInHand)
//        nbtItem.removeKey(blocksBrokenNBTKey)
//        sender.itemInHand = nbtItem.item
//        sender.sendMessage("${GREEN}Your pickaxe has been cleaned! Break a block and the lore should increase properly.")
//    }

    @Suppress("DEPRECATION")
    @HelpCommand
    @Subcommand("help")
    fun help(sender: CommandSender)
    {
        showCommandHelp()
    }
}
