package me.bristermitten.blocks.lang

import me.clip.placeholderapi.PlaceholderAPI
import org.bukkit.OfflinePlayer
import org.bukkit.command.CommandSender
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.entity.Player


class YamlLangService(private val file: YamlConfiguration) : LangService
{

    companion object
    {
        private const val DEFAULT_VALUE = "undefined message"
    }

    override fun getFormattedMessage(langKey: LangKey, player: OfflinePlayer?): String
    {
        val message = file.getString(langKey.key) ?: DEFAULT_VALUE

        return PlaceholderAPI.setPlaceholders(player, message.color())
    }

    override fun getFormattedList(langKey: LangKey, player: OfflinePlayer?): List<String>
    {
        var messages =
                if (file.isList(langKey.key)) file.getStringList(langKey.key)
                else listOf(file.getString(langKey.key) ?: DEFAULT_VALUE)

        messages = messages.map(String::color).map {
            PlaceholderAPI.setPlaceholders(player, it)
        }
        return messages

    }

    override fun sendFormattedMessage(receiver: CommandSender, langKey: LangKey)
    {
        val messages = getFormattedList(langKey, if (receiver is Player) receiver else null, )
        messages.forEach(receiver::sendMessage)
    }

}
