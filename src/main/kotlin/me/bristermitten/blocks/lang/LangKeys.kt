package me.bristermitten.blocks.lang

enum class LangKeys : LangKey
{
    BLOCKS_BROKEN,
    OTHER_BLOCKS_BROKEN,
    OTHER_BLOCKS_SET
    ;

    override val key: String

    constructor(mainKey: String)
    {
        this.key = mainKey
    }

    constructor()
    {
        this.key = name.toLowerCase().replace("_", "-")
    }


}
