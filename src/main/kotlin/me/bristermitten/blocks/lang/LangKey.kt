package me.bristermitten.blocks.lang

interface LangKey
{
    val key: String
}
