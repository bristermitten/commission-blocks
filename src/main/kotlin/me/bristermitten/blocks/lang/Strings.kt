package me.bristermitten.blocks.lang

import org.bukkit.ChatColor

fun String.color(): String = ChatColor.translateAlternateColorCodes('&', this)
fun String.stripColor(): String = ChatColor.stripColor(this)
fun String.stripHumanColors(): String = color().stripColor()
