package me.bristermitten.blocks.lang

import org.bukkit.OfflinePlayer
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

interface LangService
{
    fun getFormattedMessage(langKey: LangKey, player: OfflinePlayer?): String

    fun getFormattedList(langKey: LangKey, player: OfflinePlayer?): List<String>

    fun sendFormattedMessage(receiver: CommandSender, langKey: LangKey)
}
