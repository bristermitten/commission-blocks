/*
 * MIT License
 *
 * Copyright (c) 2020 Alexander Wood
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package me.bristermitten.blocks.data

import com.okkero.skedule.BukkitDispatcher
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin
import java.util.*
import java.util.concurrent.ConcurrentHashMap

class PlayerDataStorage(private val dao: PlayerDataDAO, private val plugin: JavaPlugin)
{
    private val data = ConcurrentHashMap<UUID, PlayerData>()

    init
    {
        GlobalScope.launch(BukkitDispatcher(plugin, async = true)) {
            val loadedData = dao.loadAll()
                    .map {
                        it.uuid to it
                    }.toMap()
            data.putAll(loadedData)
        }

    }


    operator fun get(player: Player): PlayerData = get(player.uniqueId)

    operator fun get(uuid: UUID): PlayerData
    {
        return data.getOrPut(uuid) {
            PlayerData(uuid)
        }
    }

    val all: Set<PlayerData>
        get() = data.values.toSet()


    fun queueUpdate(data: PlayerData, onComplete: (PlayerData) -> Unit = {})
    {
        GlobalScope.launch(BukkitDispatcher(plugin, async = true)) {
            val loaded = dao.load(data.uuid)
            loaded?.let {
                data.blocks = it.blocks
                onComplete(it)
            }
        }
    }


    fun save(data: PlayerData)
    {
        GlobalScope.launch(BukkitDispatcher(plugin, async = true)) {
            dao.save(data)
        }
    }

    suspend fun saveAll()
    {
        dao.saveAll(data.values.toSet())
    }

    suspend fun shutdown()
    {
        dao.shutdown()
    }
}
