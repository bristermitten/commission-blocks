/*
 * MIT License
 *
 * Copyright (c) 2020 Alexander Wood
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package me.bristermitten.blocks.data

import com.okkero.skedule.BukkitDispatcher
import kotlinx.coroutines.*
import me.bristermitten.blocks.Blocks
import me.bristermitten.blocks.config.BlocksConfig
import java.util.concurrent.TimeUnit

class SaveIntervalTask(
        private val plugin: Blocks,
        private val storage: PlayerDataStorage,
        blocksConfig: BlocksConfig,
)
{
    private val delayTime = TimeUnit.MINUTES.toMillis(blocksConfig.saveInterval.toLong()) * 20

    private lateinit var job: Job

    fun start()
    {
        job = GlobalScope.launch(BukkitDispatcher(plugin, async = false)) {
            while (isActive)
            {
                run()
                delay(delayTime)
            }
        }
    }

    fun stop()
    {
        GlobalScope.launch {
            job.cancelAndJoin()
        }
    }

    private suspend fun run()
    {
        storage.saveAll()
    }
}
