/*
 * MIT License
 *
 * Copyright (c) 2020 Alexander Wood
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package me.bristermitten.blocks.data

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.bukkit.configuration.file.YamlConfiguration
import java.io.File
import java.util.*

class YamlPlayerDataDAO(private val file: File) : PlayerDataDAO
{

    override suspend fun saveAll(data: Set<PlayerData>)
    {
        val yamlConfiguration = YamlConfiguration.loadConfiguration(file)
        val blocks = yamlConfiguration.createSection("blocks")
        for (player in data)
        {
            blocks[player.uuid.toString()] = player.blocks
        }

        withContext(Dispatchers.IO) {
            yamlConfiguration.save(file)
        }
    }

    override suspend fun shutdown()
    {
        //No op in yaml
    }

    override suspend fun loadAll(): Set<PlayerData>
    {

        val config = YamlConfiguration.loadConfiguration(file)

        val blocksConfig = config.getConfigurationSection("blocks") ?: return emptySet()

        return blocksConfig.getKeys(false).map { key ->
            val uuid = UUID.fromString(key)
            val blocks = blocksConfig.getLong(key)

            PlayerData(uuid, blocks)
        }.toSet()
    }

    override suspend fun save(data: PlayerData)
    {
        val yamlConfiguration = YamlConfiguration.loadConfiguration(file)
        val blocksConfig = yamlConfiguration.getConfigurationSection("blocks")
        blocksConfig[data.uuid.toString()] = data.blocks
        withContext(Dispatchers.IO) {
            yamlConfiguration.save(file)
        }
    }

    override suspend fun load(uuid: UUID): PlayerData?
    {
        val yamlConfiguration = YamlConfiguration.loadConfiguration(file)
        val blocksConfig = yamlConfiguration.getConfigurationSection("blocks")
        if (uuid.toString() !in blocksConfig)
        {
            return null
        }
        val blocks = blocksConfig.getLong(uuid.toString())

        return PlayerData(uuid, blocks)
    }
}
