/*
 * MIT License
 *
 * Copyright (c) 2020 Alexander Wood
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package me.bristermitten.blocks.sql

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import me.bristermitten.blocks.config.DBConfig
import org.intellij.lang.annotations.Language
import java.sql.PreparedStatement
import java.sql.ResultSet

class SQLConnector(
        private val dbConfig: DBConfig,
)
{
    private val dataSource: HikariDataSource


    init
    {
        Class.forName("com.mysql.jdbc.Driver")
        val config = HikariConfig()

        config.jdbcUrl = dbConfig.jdbcUrl
        config.username = dbConfig.username
        config.password = dbConfig.password

        config.leakDetectionThreshold = 10000
        config.maximumPoolSize = 10

        this.dataSource = HikariDataSource(config)
    }

    fun execute(preparedStatement: PreparedStatement)
    {
        preparedStatement.execute()
        preparedStatement.close()
    }


   fun <T> prepareAndQuery(@Language("SQL") statement: String, initStatement: PreparedStatement.() -> Unit = {}, queryExtractor: (ResultSet) -> T): T
    {
        connection.use {
            it.prepareStatement(statement.replace("%prefix%", dbConfig.prefix)).use { preparedStatement ->
                initStatement(preparedStatement)

                val results = preparedStatement.executeQuery()
                return queryExtractor(results)
            }
        }
    }

    fun prepareAndUpdate(@Language("SQL") statement: String, initStatement: PreparedStatement.() -> Unit = {})
    {
        connection.use {
            it.prepareStatement(statement.replace("%prefix%", dbConfig.prefix)).use { preparedStatement ->
                initStatement(preparedStatement)
                preparedStatement.executeUpdate()
            }
        }
    }

    fun shutdown()
    {
        dataSource.close()
    }

    private val connection
        get() = dataSource.connection
}
