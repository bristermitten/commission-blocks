/*
 * MIT License
 *
 * Copyright (c) 2020 Alexander Wood
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package me.bristermitten.blocks.sql

import com.okkero.skedule.SynchronizationContext.ASYNC
import com.okkero.skedule.schedule
import me.bristermitten.blocks.data.PlayerData
import me.bristermitten.blocks.data.PlayerDataDAO
import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.plugin.java.JavaPlugin
import java.sql.ResultSet
import java.util.*

class SQLPlayerDataDAO(
        private val sqlConnector: SQLConnector,
        plugin: JavaPlugin,
) : PlayerDataDAO
{
    init
    {
        Bukkit.getScheduler().schedule(plugin, ASYNC) {
            sqlConnector.prepareAndUpdate(
                    """
                CREATE TABLE IF NOT EXISTS playerblocks
                (
                %prefix%uuid varchar(36) primary key unique,
                %prefix%blocksmined      int default 0
                )
                """.trimMargin())
        }
    }

    override suspend fun saveAll(data: Set<PlayerData>)
    {
        data.forEach { save(it) }
    }

    override suspend fun loadAll(): Set<PlayerData>
    {
        return sqlConnector.prepareAndQuery("SELECT * From playerblocks") {
            val set = mutableSetOf<PlayerData>()
            while (it.next())
            {
                set.add(loadPlayerData(it))
            }
            set
        }

    }


    override suspend fun save(data: PlayerData)
    {
        sqlConnector.prepareAndUpdate(
                """
                INSERT INTO `playerblocks`(%prefix%uuid, %prefix%blocksmined)
                VALUES (?, ?)
                ON DUPLICATE KEY UPDATE %prefix%blocksmined=?
                """.trimMargin())
        {
            setString(1, data.uuid.toString())
            setLong(2, data.blocks)
            setLong(3, data.blocks)
        }
    }

    override suspend fun load(uuid: UUID): PlayerData?
    {
        return sqlConnector.prepareAndQuery(
                """
                    SELECT * FROM playerblocks WHERE %prefix%uuid = ?
                """.trimIndent(),
                {
                    setString(1, uuid.toString())
                }
        ) {
            if (it.next())
            {
                loadPlayerData(it)
            } else
            {
                null
            }
        }
    }


    private fun loadPlayerData(resultSet: ResultSet): PlayerData
    {
        val id = UUID.fromString(resultSet.getString(1))
        val blocks = resultSet.getLong(2)

        return PlayerData(id, blocks)
    }

    override suspend fun shutdown()
    {
        sqlConnector.shutdown()
    }
}
