package me.bristermitten.blocks

import co.aikar.commands.PaperCommandManager
import kotlinx.coroutines.runBlocking
import me.bristermitten.blocks.commands.BlocksCommand
import me.bristermitten.blocks.config.BlocksConfig
import me.bristermitten.blocks.data.PlayerDataStorage
import me.bristermitten.blocks.data.SaveIntervalTask
import me.bristermitten.blocks.data.YamlPlayerDataDAO
import me.bristermitten.blocks.event.BreakHandler
import me.bristermitten.blocks.event.BukkitBlockBreakListener
import me.bristermitten.blocks.event.JoinLeaveListener
import me.bristermitten.blocks.event.TEBlockBreakListener
import me.bristermitten.blocks.lang.LangService
import me.bristermitten.blocks.lang.YamlLangService
import me.bristermitten.blocks.placeholders.BlocksPlaceholder
import me.bristermitten.blocks.sql.SQLConnector
import me.bristermitten.blocks.sql.SQLPlayerDataDAO
import org.bukkit.Bukkit
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.plugin.java.JavaPlugin
import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.constructor.CustomClassLoaderConstructor

class Blocks : JavaPlugin()
{
    private lateinit var storage: PlayerDataStorage
    private val yaml = Yaml(CustomClassLoaderConstructor(BlocksConfig::class.java.classLoader))
    private lateinit var config: BlocksConfig
    private lateinit var saveTask: SaveIntervalTask
    private lateinit var langService: LangService

    override fun onEnable()
    {
        saveDefaultConfig()

        Bukkit.getConsoleSender()
        config = yaml.loadAs(getConfig().saveToString(), BlocksConfig::class.java)

        loadData()

        val pluginManager = server.pluginManager
        val breakHandler = BreakHandler(storage, config)


        pluginManager.registerEvents(BukkitBlockBreakListener(breakHandler), this)
        pluginManager.registerEvents(TEBlockBreakListener(breakHandler), this)
        pluginManager.registerEvents(JoinLeaveListener(storage), this)

        saveTask = SaveIntervalTask(this, storage, config)
        saveTask.start()

        val langFile = dataFolder.resolve("lang.yml")
        if (!langFile.exists())
        {
            saveResource("lang.yml", false)
        }
        this.langService = YamlLangService(YamlConfiguration.loadConfiguration(langFile))

        val manager = PaperCommandManager(this)
        @Suppress("DEPRECATION")
        manager.enableUnstableAPI("help")
        manager.registerCommand(BlocksCommand(storage, langService))

        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null)
        {
            BlocksPlaceholder(this, storage).register()
            println("REGISTERED")
        }
    }


    override fun onDisable()
    {
        saveTask.stop()
        saveData()
    }

    private fun saveData()
    {
        runBlocking {
            storage.saveAll()
            storage.shutdown()
        }
    }

    private fun loadData()
    {
        val dao = if (config.database.enabled)
        {
            SQLPlayerDataDAO(SQLConnector(config.database), this)
        } else
        {
            YamlPlayerDataDAO(dataFolder.resolve("data.yml"))
        }

        storage = PlayerDataStorage(dao, this)
    }
}
