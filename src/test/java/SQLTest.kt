/*
 * MIT License
 *
 * Copyright (c) 2020 Alexander Wood
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import be.seeseemelk.mockbukkit.MockBukkit
import kotlinx.coroutines.runBlocking
import me.bristermitten.blocks.config.DBConfig
import me.bristermitten.blocks.data.PlayerDataStorage
import me.bristermitten.blocks.sql.SQLConnector
import me.bristermitten.blocks.sql.SQLPlayerDataDAO
import org.junit.After
import org.junit.Assert.assertTrue
import org.junit.Test
import java.util.*

class SQLTest
{


    @Test
    fun testSQL()
    {
        MockBukkit.mock()
        val plugin = MockBukkit.createMockPlugin()
        val config = DBConfig(true, "localhost", 3306, "blocks", "blocks", "root", "password")
        val connector = SQLConnector(config)
        val dao = SQLPlayerDataDAO(connector, plugin)
        val playerDataStorage = PlayerDataStorage(dao, plugin)


        val data = playerDataStorage[UUID.randomUUID()]
        val all = playerDataStorage.all

        assertTrue(data in all)

        runBlocking {
            playerDataStorage.saveAll()
        }
    }

    @After
    fun teardown()
    {
        MockBukkit.unload()
    }
}
