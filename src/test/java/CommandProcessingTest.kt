import be.seeseemelk.mockbukkit.MockBukkit
import co.aikar.commands.PaperCommandManager
import me.bristermitten.blocks.commands.BlocksCommand
import me.bristermitten.blocks.data.PlayerDataStorage
import me.bristermitten.blocks.data.YamlPlayerDataDAO
import org.bukkit.plugin.java.JavaPlugin
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/*
 * MIT License
 *
 * Copyright (c) 2020 Alexander Wood
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

class CommandProcessingTest
{
    private lateinit var dataStorage: PlayerDataStorage
    private lateinit var plugin: JavaPlugin


    @Before
    fun setUp()
    {
        MockBukkit.mock()
        plugin = MockBukkit.createMockPlugin()
        val dao = YamlPlayerDataDAO(plugin.dataFolder.resolve("data.yml"))
        dataStorage = PlayerDataStorage(dao, plugin)

        val manager = PaperCommandManager(plugin)
        manager.registerCommand(BlocksCommand(dataStorage, ))
    }

    @After
    fun tearDown()
    {
        MockBukkit.unload()
    }

    @Test
    fun testBaseCommandExecution()
    {
        val player = MockBukkit.getMock().addPlayer()
        player.performCommand("blocks")

        assertTrue(dataStorage[player].blocks.toString() in player.nextMessage())
    }

    @Test
    fun testOtherPlayerCommandExecution()
    {
        val player = MockBukkit.getMock().addPlayer()
        val target = MockBukkit.getMock().addPlayer()
        player.performCommand("blocks ${target.name}")

        val message = player.nextMessage()
        assertTrue(dataStorage[target].blocks.toString() in message)
        assertTrue(target.name in message)
    }

    @Test
    fun testAdminCommandWithoutPermission()
    {
        val player = MockBukkit.getMock().addPlayer()
        val target = MockBukkit.getMock().addPlayer()
        player.performCommand("blocks set ${target.name} 30")

        val message = player.nextMessage()
        assertNotEquals(dataStorage[target].blocks, 30) //No permission
    }

    @Test
    fun testAdminCommandWithPermission()
    {
        val player = MockBukkit.getMock().addPlayer()
        player.addAttachment(plugin).setPermission("blocks.set", true)

        val target = MockBukkit.getMock().addPlayer()

        player.performCommand("blocks set ${target.name} 30")

        assertEquals(dataStorage[target].blocks, 30) //Has permission so the blocks are set
    }
}
