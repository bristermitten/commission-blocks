/*
 * MIT License
 *
 * Copyright (c) 2020 Alexander Wood
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import me.bristermitten.blocks.config.BlocksConfig
import org.junit.Assert.assertEquals
import org.junit.Test
import org.yaml.snakeyaml.Yaml

class YamlTest
{
    private val yaml = """
database:
  enabled: true
  hostname: test_hostname
  port: 3306
  database: test_db
  prefix: test_prefix
  username: test_username
  password: test_password
saveInterval: 10
    """.trimIndent()


    @Test
    fun testLoading()
    {
        val config = Yaml().loadAs(yaml, BlocksConfig::class.java)
        assertEquals(true, config.database.enabled)
        assertEquals("test_hostname", config.database.hostname)
        assertEquals(3306, config.database.port)
        assertEquals("test_db", config.database.database)
        assertEquals("test_prefix", config.database.prefix)
        assertEquals("test_username", config.database.username)
        assertEquals("test_password", config.database.password)
        assertEquals(10, config.saveInterval)
    }

}
